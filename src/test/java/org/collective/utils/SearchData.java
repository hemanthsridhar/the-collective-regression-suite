package org.collective.utils;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;


@Resource.Classpath("org/collective/utils/SearchData.properties")
public class SearchData {

	public SearchData() {
		PropertyLoader.populate(this);
	}
	
	@Property("logoutMsg")
	private  String logoutMsg;
	
	@Property("signInPhoneNumber")
	private  String signInPhoneNumber;
	
	@Property("signInPassword")
	private  String signInPassword;
	
	@Property("adminSignInPhoneNumber")
	private  String adminSignInPhoneNumber;
	
	@Property("adminSignInPassword")
	private  String adminSignInPassword;
	
	@Property("cartFirstName")
	private  String cartFirstName;
	
	@Property("cartLastName")
	private  String cartLastName;
	
	@Property("cartStreetAddress")
	private  String cartStreetAddress;
	
	@Property("cartPincode")
	private  String cartPincode;
	
	@Property("cartCity")
	private  String cartCity;
	
	@Property("cartState")
	private  String cartState;
	
	@Property("cartCountry")
	private  String cartCountry;
	
	@Property("cartPhoneNumber")
	private  String cartPhoneNumber;
	
	@Property("orderStatus")
	private  String orderStatus;
	
	@Property("signupUsername")
	private  String signupUsername;
	
	@Property("signupEmail")
	private  String signupEmail;
	
	@Property("signupPassword")
	private  String signupPassword;
	
	@Property("adminPageURL")
	private  String adminPageURL;
	
	@Property("pageURLs")
	private String getURL;
	
	@Property("pageURLsStaging")
	private String pageURLsStaging;
	
	@Property("FAQHeader")
	private String FAQHeader;
	
	@Property("ShippingPolicyHeader")
	private String ShippingPolicyHeader;
	
	@Property("ReturnsHeader")
	private String returnsHeader;
	
	@Property("SocialNetworksURLs")
	private String SocialNetworksURLs;
	
	@Property("storeNames")
	private String storeNames;
	
	@Property("eventHeader")
	private String eventHeader;
	
	@Property("careersText")
	private String careersText;
	
	@Property("aboutUsText")
	private String aboutUsText;
	
	@Property("faqText")
	private String faqText;
	
	@Property("filterBrandAssert")
	private String filterBrandAssert;
	
	@Property("size")
	private String size;
	
	@Property("colour")
	private String colour;
	
	@Property("NumberOfProductsForQuantityTC028")
	private String NumberOfProductsForQuantityTC028;
	
	@Property("NumberOfTimesToAddDifferentProductTC029")
	private int NumberOfTimesToAddDifferentProductTC029;
	
	@Property("alertErrorMsgNoUsernameAndNoPwdTC034")
	private String alertErrorMsgNoUsernameAndNoPwdTC034;
	
	@Property("alertErrorMsgUsernameAndNoPwdTC035")
	private String alertErrorMsgUsernameAndNoPwdTC035;
	
	@Property("alertErrorMsgNoUsernameAndPwdTC036")
	private String alertErrorMsgNoUsernameAndPwdTC036;
	
	@Property("alertErrorMsgInvalidUsernameAndPwdTC037")
	private String alertErrorMsgInvalidUsernameAndPwdTC037;
	
	@Property("invalidUsername")
	private String invalidUsername;
	
	@Property("invalidPassword")
	private String invalidPassword;
	
	@Property("errorMsgForRegAllEmpty")
	private String errorMsgForRegAllEmpty;
	
	@Property("errorMsgForRegExistingPhnoAndPwd")
	private String errorMsgForRegExistingPhnoAndPwd;
	
	@Property("errorMsgForRegPwdMismatch")
	private String errorMsgForRegPwdMismatch;
	
	@Property("introText")
	private String introText;
	
	@Property("blogNextClass")
	private String blogNextClass;
	
	@Property("blogNextClassAfterPrevious")
	private String blogNextClassAfterPrevious;
	
	@Property("specialCharacters")
	private String specialCharacters;
	
	@Property("wholeNumbers")
	private String wholeNumbers;
	
	@Property("existingEmail")
	private String existingEmail;
	
	@Property("signUpPhoneNumberLessThan10")
	private String signUpPhoneNumberLessThan10;
	
	@Property("hugeNumberForQuantityTC054")
	private String hugeNumberForQuantityTC054;
	
	@Property("verifyErrorMsgForHugeQuantity")
	private String verifyErrorMsgForHugeQuantity;
	
	@Property("negativeNumberForQuantityTC055")
	private String negativeNumberForQuantityTC055;
	
	@Property("decimalnumberForQuantityTC056")
	private String decimalnumberForQuantityTC056;
	
	@Property("madeToMeasureFirstPageText")
	private String madeToMeasureFirstPageText;
	
	@Property("madeToMeasureAreMadeEqualText")
	private String madeToMeasureAreMadeEqualText;
	
	@Property("madeToMeasureCollectiveAtelierText")
	private String madeToMeasureCollectiveAtelierText;
	
	@Property("armaniPageText")
	private String armaniPageText;
	
	@Property("santoniText")
	private String santoniText;
	
	@Property("etonText")
	private String etonText;
	
	@Property("hackettText")
	private String hackettText;
	
	@Property("makeAnAppointmentHeadingText")
	private String makeAnAppointmentHeadingText;
	
	@Property("appointmentsContactText")
	private String appointmentsContactText;
	
	@Property("appointmentConfirmationText")
	private String appointmentConfirmationText;
	
	@Property("armaniTextTabletView")
	private String armaniTextTabletView;
	
	@Property("armaniYoutubeText")
	private String armaniYoutubeText;
	
	@Property("santoniYoutubeText")
	private String santoniYoutubeText;
	
	@Property("appointmentEmailErrorMsg")
	private String appointmentEmailErrorMsg;
	
	@Property("appointmentPhoneNumberErrorMsg")
	private String appointmentPhoneNumberErrorMsg;
	
	@Property("signUpPhoneNumberMoreThan10")
	private String signUpPhoneNumberMoreThan10;
	
	@Property("errorMsgsForAppointmentsAllBlank")
	private String errorMsgsForAppointmentsAllBlank;
	
	@Property("appointmentNameBlankErrorMsg")
	private String appointmentNameBlankErrorMsg;
	
	@Property("appointmentEmailBlankErrorMsg")
	private String appointmentEmailBlankErrorMsg;

	@Property("appointmentPhoneNumberBlankErrorMsg")
	private String appointmentPhoneNumberBlankErrorMsg;
	
	@Property("errorMsgBlankPhoneNumberOrPwdOrBothBlank")
	private String errorMsgBlankPhoneNumberOrPwdOrBothBlank;
	
	@Property("createStoreCity")
	private String createStoreCity;
	
	@Property("createStoreAddress1")
	private String createStoreAddress1;
	
	@Property("createStoreAddress2")
	private String createStoreAddress2;
	
	@Property("blogTitle")
	private String blogTitle;
	
	@Property("blogBody")
	private String blogBody;
	
	@Property("blogSummary")
    private String blogSummary;
	
	@Property("blogImageFilePath")
	private String blogImageFilePath;
	
	@Property("blogEntryDeleteAlert")
	private String blogEntryDeleteAlert;
	
	@Property("blogEntryCreateAlert")
	private String blogEntryCreateAlert;
	
	@Property("userCreatePhoneNumber")
	private String userCreatePhoneNumber;
	
	@Property("userCreateSuccessMsg")
	private String userCreateSuccessMsg;
	
	@Property("userDeleteSuccessMsg")
	private String userDeleteSuccessMsg;
	
	@Property("promotionsName")
	private String promotionsName;
	
	@Property("promotionsCode")
	private String promotionsCode;
	
	@Property("promotionsDescription")
	private String promotionsDescription;
	
	@Property("promotionsUsageLimit")
	private String promotionsUsageLimit;
	
	@Property("analyticScriptName")
	private String analyticScriptName;
	
	@Property("analyticScriptCode")
	private String analyticScriptCode;
	
	@Property("orderCancellationText")
	private String orderCancellationText;
	
	@Property("carouselName")
	private String carouselName;
	
	@Property("carouselCoverFilePath")
	private String carouselCoverFilePath;
	
	@Property("carouselSmallImagePath")
	private String carouselSmallImagePath;
	
	@Property("carouselDescription")
	private String carouselDescription;
	
	@Property("carouselLink")
	private String carouselLink;
	
	@Property("taxonLegendText")
	private String taxonLegendText;
	
	@Property("cartInvalidPincode")
	private String cartInvalidPincode;
	
	@Property("subscribeNewsLetterText")
	private String subscribeNewsLetterText;
	
	@Property("subscribeNewsLetterTextExistingEmail")
	private String subscribeNewsLetterTextExistingEmail;
	
	public  String getSignUpPhoneNumberMoreThan10(){
		return signUpPhoneNumberMoreThan10;
	}
	
	public String getLogoutMsg() {
		return logoutMsg;
	}	
	
	public String getsignInPhoneNumber() {
		return signInPhoneNumber;
	}
	
	public String getsignInPassword() {
		return signInPassword;
	}
	
	public String getadminSignInPhoneNumber() {
		return adminSignInPhoneNumber;
	}
	
	public String getadminadminSignInPassword() {
		return adminSignInPassword;
	}
	
	public String getcartFirstName() {
		return cartFirstName;
	}
	
	public String getcartLastName() {
		return cartLastName;
	}
	
	public String getcartStreetAddress() {
		return cartStreetAddress;
	}
	
	public String getcartPincode() {
		return cartPincode;
	}
	
	public String getcartCity() {
		return cartCity;
	}
	
	public String getcartState() {
		return cartState;
	}
   
	public String getcartCountry() {
		return cartCountry;
	}
	
	public String getcartPhoneNumber() {
		return cartPhoneNumber;
	}
	
	public String getorderStatus() {
		return orderStatus;
	}
	
	public String getsignupUsername() {
		return signupUsername;
	}
	
	public String getsignupEmail() {
		return signupEmail;
	}
	
	public String getsignupPassword() {
		return signupPassword;
	}
	
	public String getadminPageURL() {
		return adminPageURL;
	}
	
	public String getPageURLs(){
		return getURL;
	}
	
	public String getPageURLsStaging(){
		return pageURLsStaging;
	}
	
	public String getFAQHeader(){
		return FAQHeader;
	}

	public String getShippingHeader() {
		return ShippingPolicyHeader;
	}

	public String getReturnsHeader() {
		
		return returnsHeader;
	}

	public String getSocialNetworksURLs() {
		
		return SocialNetworksURLs;
	}

	public String getStores() {
		
		return storeNames;
	}

	public String getEventHeader() {
		
		return eventHeader;
	}

	public String getCareersText() {
		return careersText;
	}	
	
	public String getAboutUsText(){
		return aboutUsText;
	}
	
	public String getFaqText(){
		return faqText;
	}

	public String getFilterBrandAssert() {
		return filterBrandAssert;
	}

	public String getSize() {
		return size;
		
	}

	public String getColour() {
		
		return colour;
	}

	public String getNumberOfProductsForQuantityTC028() {
		
		return NumberOfProductsForQuantityTC028;
	}

	public int getNumberOfTimesToAddDifferentProduct() {
		
		return NumberOfTimesToAddDifferentProductTC029;
	}

	public String getalertErrorMsgNoUsernameAndPwdTC034() {
		
		return alertErrorMsgNoUsernameAndNoPwdTC034;
	}

	public String getalertErrorMsgNoUsernameAndPwdTC035() {
		
		return alertErrorMsgUsernameAndNoPwdTC035;
	}

	public String getalertErrorMsgNoUsernameAndPwdTC036() {
		
		return alertErrorMsgNoUsernameAndPwdTC036;
	}

	public String getAlertErrorMsgInvalidUsernameAndPwdTC037(){
		return alertErrorMsgInvalidUsernameAndPwdTC037;
	}
	
	public String getInvalidSignInPhoneNumber() {
		return invalidUsername;
	}

	public String getInvalidSignInPassword() {
		return invalidPassword;
	}

	public String geterrorMsgForRegAllEmpty() {
		
		return errorMsgForRegAllEmpty;
	}

	public String geterrorMsgForRegExistingPhnoAndPwd() {
	
		return errorMsgForRegExistingPhnoAndPwd;
	}

	public String geterrorMsgForRegPwdMismatch() {

		return errorMsgForRegPwdMismatch;
	}
	
	public String getIntroText(){
		return introText;
	}
	
	public String getBlogNextClass(){
		return blogNextClass;
	}

	public String getBlogNextClassAfterPrevious() {

		return blogNextClassAfterPrevious;
	}

	public String getWholeNumbers() {
		return wholeNumbers;
	}

	public String getSpecialCharacters() {
		
		return specialCharacters;
	}

	public String getExistingMail() {
	
		return existingEmail;
	}

	public String getSignUpPhoneNumberLessThan10() {
		
		return signUpPhoneNumberLessThan10;
	}

	public String getHugeNumberForQuantityTC054() {
	
		return hugeNumberForQuantityTC054;
	}

	public String getVerifyErrorMsgForHugeQuantity() {
	
		return verifyErrorMsgForHugeQuantity;
	}

	public String getNegativeNumberForQuantityTC055() {
		
		return negativeNumberForQuantityTC055;
	}

	public String getDecimalNumberForQuantityTC056() {
		
		return decimalnumberForQuantityTC056;
	}

	public String getMadeToMeasureFirstPageText() {
		
		return madeToMeasureFirstPageText;
	}

	public String getMadeToMeasureAreMadeEqualText() {
		
		return madeToMeasureAreMadeEqualText;
	}

	public String getTheCollectiveAtelierText() {
		
		return madeToMeasureCollectiveAtelierText;
	}

	public String getArmaniPageText() {
		
		return armaniPageText;
	}

	public String getSantoniPageText() {
		
		return santoniText;
	}

	public String getEtonPageText() {
		
		return etonText;
	}

	public String getHackettPageText() {
		
		return hackettText;
	}

	public String getAppointmentsHeadingText() {
		
		return makeAnAppointmentHeadingText;
	}

	public String getAppointmentsContactText() {
		
		return appointmentsContactText;
	}

	public String getAppointmentConfirmationText() {
		
		return appointmentConfirmationText;
	}

	public String getArmaniPageTextTableView() {
		
		return armaniTextTabletView;
	}

	public String getArmaniYoutubeDescription() {
	
		return armaniYoutubeText;
	}

	public String getSantoniYoutubeDescription() {
		
		return santoniYoutubeText;
	}

	public String getAppointmentEmailErrorMsg() {
		
		return appointmentEmailErrorMsg;
	}

	public String getAppointmentPhoneNumberErrorMsg() {
		
		return appointmentPhoneNumberErrorMsg;
	}

	public String getErrorMsgsForAppointmentsAllBlank() {
		
		return errorMsgsForAppointmentsAllBlank;
	}

	public String getAppointmentNameBlankErrorMsg() {
		
		return appointmentNameBlankErrorMsg;
	}

	public String getAppointmentEmailBlankErrorMsg() {
		
		return appointmentEmailBlankErrorMsg;
	}

	public String getAppointmentPhoneNumberBlankErrorMsg() {
		
		return appointmentPhoneNumberBlankErrorMsg;
	}

	public String getAlertErrorMsgBlankPhoneNumberOrPwdOrBothBlank() {
		
		return errorMsgBlankPhoneNumberOrPwdOrBothBlank;
	}

	public String getCreateStoreCity() {
		
		return createStoreCity;
	}

	public String getCreateStoreAddress1() {
		
		return createStoreAddress1;
	}

	public String getCreateStoreAddress2() {
	
		return createStoreAddress2;
	}

	public String getblogTitle() {
		
		return blogTitle;
	}

	public String getBlogSummary() {
		
		return blogSummary;
	}

	public String getblogBody() {
		
		return blogBody;
	}

	public String getBlogImageFilePath() {
	
		return blogImageFilePath;
	}

	public String getBlogEntryDeleteAlert() {

		return blogEntryDeleteAlert;
	}

	public String getBlogEntryCreate() {
		
		return blogEntryCreateAlert;
	}

	public String getUserCreatePhoneNumber() {
		
		return userCreatePhoneNumber;
	}

	public String getUserCreateSuccessMsg() {
		
		return userCreateSuccessMsg;
	}

	public String getUserDeleteSuccessMsg() {
		
		return userDeleteSuccessMsg;
	}

	public String getPromotionsName() {
		
		return promotionsName;
	}

	public String getPromotionsCode() {
		
		return promotionsCode;
	}

	public String getPromotionsDescription() {
		
		return promotionsDescription;
	}

	public String getUsageLimit() {
		
		return promotionsUsageLimit;
	}

	public String getAnalyticScriptName() {
		
		return analyticScriptName;
	}

	public String getAnalyticScriptCode() {
		
		return analyticScriptCode;
	}

	public String getOrderCancellationText() {
		
		return orderCancellationText;
	}

	public String getCaraouselName() {
		
		return carouselName;
	}

	public String getCarouselFilePath() {
		
		return carouselCoverFilePath;
	}

	public String getCarouselSmallImagePath() {
		
		return carouselSmallImagePath;
	}
	
	public String getCarouselDescription(){
		return carouselDescription;
	}
	
	public String getCarouselLink(){
		return carouselLink;
	}

	public String getTaxonLegendText() {
		
		return taxonLegendText;
	}

	public String getcartInvalidPincode() {
		
		return cartInvalidPincode;
	}

	public String getSubscribeNewsLetterText() {
		// TODO Auto-generated method stub
		return subscribeNewsLetterText;
	}

	public String getSubscribeNewsLetterTextExistingEmail() {
		// TODO Auto-generated method stub
		return subscribeNewsLetterTextExistingEmail;
	}
}
