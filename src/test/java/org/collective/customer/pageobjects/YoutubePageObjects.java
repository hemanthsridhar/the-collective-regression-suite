package org.collective.customer.pageobjects;

import org.collective.maincontroller.MainController;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YoutubePageObjects extends MainController {

	public YoutubePageObjects(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[@id='eow-title']")
	private WebElement videoDescription;
	
	CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);

	public boolean assertArmaniVideoHeading() {
		
		Boolean t = videoDescription.getText().trim().equals(data.getArmaniYoutubeDescription());
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		return t;
		}
		
		

	public boolean assertSantoniVideoHeading() {
		
		Boolean t = videoDescription.getText().trim().equals(data.getSantoniYoutubeDescription());
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		return t;
	
}
}