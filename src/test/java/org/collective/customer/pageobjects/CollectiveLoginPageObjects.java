package org.collective.customer.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.IOException;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveLoginPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveLoginPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(xpath="//input[@id='spree_user_phone_number']")
	private WebElement phoneNumber;
	
	@FindBy(xpath="//input[@id='spree_user_password']")
	private WebElement password;
	
	
	@FindBy(xpath="//input[@value='Login']")
	private WebElement loginBtn;

	@FindBy(xpath="//div[contains(text(),'Logged in successfully')]")
	private WebElement adminLoginMsg;
	
	@FindBy(xpath="//div[@class='alert alert-notice']")
	private WebElement alertMsg;
	
	
	@FindBy(xpath="//div[@class='flash error']")
	private WebElement errorMsg;
	
	public void enterPhoneNumberForLogin() throws IOException {
		Waiting.explicitWaitVisibilityOfElement(phoneNumber, 6);
		phoneNumber.clear();
		phoneNumber.sendKeys(data.getsignInPhoneNumber());
	}
	
	public void enterPasswordForLogin() throws IOException{
		password.clear();
		password.sendKeys(data.getsignInPassword());
	}
	
	public void clickLogin(){
		Waiting.explicitWaitVisibilityOfElement(loginBtn, 3);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",loginBtn);
	}

	public void verifyAdminLoginMsg() {
		Waiting.explicitWaitVisibilityOfElement(adminLoginMsg, 6);
		Assert.assertTrue(adminLoginMsg.isDisplayed());
		
	}

	public void enterPhoneNumberForAdmin() throws IOException {
		Waiting.explicitWaitVisibilityOfElement(phoneNumber, 6);
		phoneNumber.clear();
		phoneNumber.sendKeys(data.getadminSignInPhoneNumber());
		
	}

	public void enterPasswordForAdmin() throws IOException {
	password.clear();
	password.sendKeys(data.getadminadminSignInPassword());
		
	}

	public void verifyErrorMsgNoPhoneNumberAndPwd() {
	Assert.assertEquals(alertMsg.getText().trim(), data.getalertErrorMsgNoUsernameAndPwdTC036().trim());
		
	}

	public void verifyErrorMsgPhoneNumberAndNoPwd() {
		Waiting.explicitWaitVisibilityOfElement(alertMsg, 2);
		Assert.assertEquals(alertMsg.getText().trim(), data.getalertErrorMsgNoUsernameAndPwdTC035().trim());
		
	}

	public void verifyErrorMsgNoPhoneNumberAndNoPwd() {
		Assert.assertEquals(alertMsg.getText().trim(), data.getalertErrorMsgNoUsernameAndPwdTC034().trim());
		
	}

	public void enterInvalidPhoneNumberForLogin() {
		Waiting.explicitWaitVisibilityOfElement(phoneNumber, 6);
		phoneNumber.clear();
		phoneNumber.sendKeys(data.getInvalidSignInPhoneNumber());
		
	}

	public void enterInvalidPasswordForLogin() {
		password.clear();
		password.sendKeys(data.getInvalidSignInPassword());
		
	}

	public void verifyErrorMsgInvalidPhoneNumberAndPwd() {
		Assert.assertEquals(alertMsg.getText().trim(), data.getAlertErrorMsgInvalidUsernameAndPwdTC037().trim());
		
	}

	public void verifyErrorMsgBlankPhoneNumberOrPwdOrBothBlank() {
		Assert.assertEquals(errorMsg.getText().trim(), data.getAlertErrorMsgBlankPhoneNumberOrPwdOrBothBlank().trim());
		
	}

}
