package org.collective.testmethods;

import org.collective.utils.ApplicationSetUp;
import org.collective.utils.RestCustomMethods;
import org.collective.utils.SearchData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.jayway.restassured.RestAssured;

public class CollectiveRegressionTestURL {

	@BeforeClass
	public void init(){
		ApplicationSetUp applicationSetUp = new ApplicationSetUp();
		RestAssured.baseURI=applicationSetUp.getURL();
	}
	
	@Test(alwaysRun = true)
	public void aPI_tc001_PageURLsStaging()
	{	
		SearchData data = new SearchData();
		String g[] =  data.getPageURLsStaging().split(",");
		String s= RestCustomMethods.getRequestTestURLs(g, 200);
		 Assert.assertEquals("", s,s);
	}
	
	/*@Test(alwaysRun=true)
	  public void tc019_collective_SocialNetwork_Links(){
		SearchData data = new SearchData();
		String g[] =  data.getSocialNetworksURLs().split(",");
		String s= RestCustomMethods.getRequestTestURLs(g, 200);
		 Assert.assertEquals("", s,s);
		
		System.out.println("tc019_collective_SocialNetwork_Links is pending due to vertificate issue");
	  }*/
	/*@Test(alwaysRun = true)*/
	/*@Test
	public void tc002_PageURLProd() throws Exception
	{			    
		ApplicationSetUp applicationSetUp = new ApplicationSetUp();
		RestAssured.baseURI=applicationSetUp.getProdURL();
		SearchData data = new SearchData();
		String g[] =  data.getPageURLs().split(",");
		String s= RestCustomMethods.getRequestTestURLs(g, 200);
		Assert.assertEquals("", s,s);
	}*/
}