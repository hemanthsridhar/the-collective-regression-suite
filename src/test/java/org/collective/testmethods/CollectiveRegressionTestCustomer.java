package org.collective.testmethods;

import java.awt.AWTException;
import java.io.IOException;
import java.text.ParseException;

import org.collective.admin.pageobjects.CollectiveAdminStoreLocatorPageObjects;
import org.collective.customer.pageobjects.CollectiveAdrianoGoldschmiedPageObjects;
import org.collective.customer.pageobjects.CollectiveBagsPageObjects;
import org.collective.customer.pageobjects.CollectiveBlogPageObjects;
import org.collective.customer.pageobjects.CollectiveJeansPageObjects;
import org.collective.customer.pageobjects.CollectiveHomePageObjects;
import org.collective.customer.pageobjects.CollectiveLoginPageObjects;
import org.collective.customer.pageobjects.CollectiveMadeToMeasurePageObjects;
import org.collective.customer.pageobjects.CollectiveSalesPageObjects;
import org.collective.customer.pageobjects.CollectiveShoppingCartPageObjects;
import org.collective.customer.pageobjects.CollectiveSignupPageObjects;
import org.collective.customer.pageobjects.YoutubePageObjects;
import org.collective.maincontroller.MainController;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CollectiveRegressionTestCustomer extends MainController{
	
	/*
	 * @author Hemanth.Sridhar
	 */	
	
  @Test(alwaysRun = true)
  public void tc001_collective_signup() throws IOException, InterruptedException, AWTException {
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsername();
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  homePage.verifySignupFunctionality();
	  
  }
  
  @Test(alwaysRun = true)
  public void tc002_collective_login() throws InterruptedException, IOException
  {
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
  }
  
  @Test(alwaysRun = true)
  public void tc003_collective_blog(){
	 CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	 CollectiveBlogPageObjects blogPage = new CollectiveBlogPageObjects(driver);
	 homePage.clickBlog();
	 blogPage.verifyBlogPage();	 
  }	

  @Test(alwaysRun = true)
  public void tc004_collective_accessories(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToBags();
	  bagsPage.verifyProductsDisplay();
	  
  }
  
 // filter brand, covers five_Pkt_Non_Denim_Jean 
  
  @Test(alwaysRun = true)
  public void tc005_collective_Filter_Brand(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveJeansPageObjects jeansPage = new CollectiveJeansPageObjects(driver);
	  homePage.hoverOverMenTab();
	  homePage.navigateToJeansFromMen();
	  jeansPage.assertForDifferentProduct();
	  jeansPage.adrianoGoldschmiedCheckboxClick();
	  jeansPage.filterTest();
	  jeansPage.adrianoGoldschmiedCheckboxClick();
	  jeansPage.assertForDifferentProduct();
  }
  
  @Test(alwaysRun = true)
  public void tc006_collective_accessories_pagination()
  {
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToAdrianoGoldschmiedFromMen();
	  bagsPage.verifyProductsDisplay();
	  bagsPage.verifyPagination();
  }
  
  @Test(alwaysRun = true)
  public void tc007_collective_sales() throws ParseException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveSalesPageObjects salesPage = new CollectiveSalesPageObjects(driver);
	  homePage.clickSales();
	  bagsPage.verifyProductsDisplay();
	  salesPage.assertDiscountAndActualPrice();
	  
  }
  
  @Test(alwaysRun = true)
  public void tc008_collective_orderGreaterThan20000() throws IOException, InterruptedException{
	  
	/*  covers Mens tab as well
	    cover price filtering as well*/
	   
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverOverMenTab();
	  homePage.navigateToArmaniFromMen();
	  andrianoPage.dragLeftSlider(90);
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.checkForCashOnDeliveryGreaterthan20k();
	  shoppingcartPage.clearCart();
	  
  }
  
  @Test(alwaysRun = true)
  public void tc009_collective_orderLessOrEqualThan20000() throws IOException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverOverMenTab();
	  homePage.navigateToAdrianoGoldschmiedFromMen();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.checkForCashOnDeliveryLessThanOrEqualTo20k();
	  shoppingcartPage.clearCart();
	
  }
  
  @Test(alwaysRun = true)
  public void tc010_collective_orderPlacement() throws IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToArmaniFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
  }
  
  @Test(alwaysRun = true)
  public void tc011_collective_womenTab() throws InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  homePage.hoverOverWomenTab();
	  homePage.navigateToAdrianoGoldschmiedFromWomen();
	  bagsPage.verifyProductsDisplay();
  }
  
/*  @Test(alwaysRun=true)
  public void tc012_collective_blogStories(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.verifyBlogStories();
  }*/
  
  @Test(alwaysRun=true)
  public void tc013_collective_shoppingOnline_faq(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToFAQ();
	  homePage.verifyFAQ();
  }
  
  @Test(alwaysRun=true)
  public void tc014_collective_shoppingOnline_ShippingPolicy(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToShippingPolicy();
	  homePage.verifyShippingPolicy();
  }
  
  @Test(alwaysRun=true)
  public void tc015_collective_shoppingOnline_ReturnPolicy(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToReturnPolicy();
	  homePage.verifyReturnPolicy();
  }
  
  @Test(alwaysRun=true)
  public void tc016_collective_customerServices_theCollectivePrive(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToPrive();
	  homePage.verifyPrive();
  }
  
  @Test(alwaysRun=true)
  public void tc017_collective_customerServices_PrivacyPolicy(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToPrivacyPolicy();
	  homePage.verifyPrivacyPolicy();
  }
  
  @Test(alwaysRun=true)
  public void tc018_collective_OurStoresAndEvents_StoreLocator(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToStoreLocator();
	  homePage.verifyStoreLocator();
  }
  
  @Test(alwaysRun=true)
  public void tc019_collective_OurStoresAndEvents_Events(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToEvents();
	  homePage.verifyEvents();
  }
  
  
  @Test(alwaysRun=true)
  public void tc020_collective_OurStoresAndEvents_BrandDirectory(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToBrandDirectory();
	  homePage.verifyBrandDirectory();
  }
  
  
  @Test(alwaysRun=true)
  public void tc021_collective_AboutUs_Careers(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToCareers();
	  homePage.verifyCareers();
  }
  
  @Test(alwaysRun=true)
  public void tc022_collective_AboutUs_TheCollective(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.navigateToTheCollective();
	  homePage.verifyTheCollective();
  }
  
  
   //* pending
  
  
  @Test(alwaysRun=true)
  public void tc023_collective_FilterSize() throws InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  CollectiveJeansPageObjects fivepktnonDenimJeanPage = new CollectiveJeansPageObjects(driver);
	  homePage.hoverOverMenTab();
	  homePage.navigateToArmaniFromMen();
	  fivepktnonDenimJeanPage.chooseSize();
	  fivepktnonDenimJeanPage.clickFirstProduct();
	  Thread.sleep(2500);
	  fivepktnonDenimJeanPage.verifySize();
	 
  }
  
  @Test(alwaysRun=true)
  public void tc024_collective_FilterColour() throws InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  CollectiveJeansPageObjects fivepktnonDenimJeanPage = new CollectiveJeansPageObjects(driver);
	  homePage.hoverOverMenTab();
	  homePage.navigateToJeansFromMen();
	  fivepktnonDenimJeanPage.chooseColour();
	  fivepktnonDenimJeanPage.clickFirstProduct();
	  Thread.sleep(5000);
	  fivepktnonDenimJeanPage.verifyColour();
	 
  }
  
  @Test(alwaysRun=true)
  public void tc025_collective_FilterPrice() throws InterruptedException, ParseException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveAdrianoGoldschmiedPageObjects adrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  homePage.hoverOverMenTab();
	  homePage.navigateToAdrianoGoldschmiedFromMen();
	  adrianoPage.dragLeftSlider(50);
	  adrianoPage.verifyPriceFilter();
	  adrianoPage.revertBackToTheInitalStateUsingFilter();
	 
  }
  
  @Test(alwaysRun=true)
  public void tc026_collective_Order_same_Product_Multiple_Quantities_LessThan_20000() throws InterruptedException, ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToJeansFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.addNumberForQuantity();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  homePage.logout();
	  homePage.verifyLogout();
  }

  @Test(alwaysRun=true)
  public void tc027_collective_Order_multiple_Products_DifferentCategory_singleQuantity_Each_LessThan_20000() throws InterruptedException, ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverOverMenTab();
	  homePage.navigateToJeansFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.addDifferentProductsDifferentCategoryWithSingleQuantity();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  homePage.logout();
	  homePage.verifyLogout();
  }
  

  @Test(alwaysRun=true)
  public void tc028_collective_Order_multiple_Products_SameCategory_singleQuantity_Each_LessThan_20000() throws InterruptedException, ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToJeansFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.addDifferentProductsSameCategoryWithSingleQuantity();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  homePage.logout();
	  homePage.verifyLogout();
  }
  
  @Test(alwaysRun=true)
  public void tc029_collective_Order_multiple_Products_SameCategory_singleQuantity_Each_MoreThan_20000() throws InterruptedException, ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToArmaniFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.dragLeftSlider(90); 
	  andrianoPage.addDifferentProductsDifferentCategoryWithSingleQuantityMoreThan20000();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  homePage.logout();
	  homePage.verifyLogout();
  }

  
  @Test
  public void tc030_collective_Login_ErrorScenario_NoPhoneNumberAndNoPwd() throws IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgNoPhoneNumberAndNoPwd();
  }
  
  @Test
  public void tc031_collective_Login_ErrorScenario_PhoneNumberAndNoPwd() throws IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgPhoneNumberAndNoPwd();
  }
  
  @Test
  public void tc032_collective_Login_ErrorScenario_NoPhoneNumberAndPwd() throws IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgNoPhoneNumberAndPwd();
  }
  
  @Test
  public void tc033_collective_Login_ErrorScenario_InvalidPhoneNumberAndPwd() throws IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterInvalidPhoneNumberForLogin();
	  loginPage.enterInvalidPasswordForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgInvalidPhoneNumberAndPwd();
  }
  
  @Test
  public void tc034_collective_Registration_ErrorScenario_AllEmpty() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.clearSignUpTextboxes();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegAllEmpty();
  }
  
  
  @Test
  public void tc035_collective_Registration_ErrorScenario_ExistingPhnoAndPwd() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  loginPage.enterPhoneNumberForLogin();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegExistingPhnoAndPwd();
  }
  
  @Test
  public void tc036_collective_Registration_ErrorScenario_UniquePhnoAndPwd_NoName_No_Email() throws IOException, AWTException, InterruptedException{
	 System.out.println("pending because of a bug");
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegUniquePhnoAndPwd_NoName_No_Email();
  }
  
  @Test
  public void tc037_collective_Registration_ErrorScenario_UniquePhnoAndPwd_NoName() throws IOException, AWTException, InterruptedException{
	  System.out.println("pending due to a bug");
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegUniquePhnoAndPwd_NoName_No_Email();
  }
  
  @Test
  public void tc038_collective_Registration_ErrorScenario_UniquePhnoAndPwd_NoEmail() throws IOException, AWTException, InterruptedException{
	  System.out.println("pending due to a bug");
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsername();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegUniquePhnoAndPwd_NoName_No_Email();
  }
  
  @Test
  public void tc039_collective_Registration_ErrorScenario_UniquePhno_PasswordMismatch() throws IOException, AWTException, InterruptedException{
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordForErrorScenario();
	  signupPage.signUpPasswordConfirmationForErrorScenario();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsgRegUniquePhnoPwdMismatch();
  }
  
  @Test
  public void tc040_collective_Registration_LoginAsExistingCustomer() throws IOException, AWTException, InterruptedException{
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.clickOnLoginAsExistingCustomer();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.logout();
	  homePage.verifyLogout();
  }
  
  @Test
  public void tc041_collective_HomePageIntroductionText() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.checkIntroText();
  }
  
  @Test
  public void tc042_collective_BlogPageCompleteAssertion(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
		 CollectiveBlogPageObjects blogPage = new CollectiveBlogPageObjects(driver);
		 homePage.clickBlog();
		 blogPage.verifyBlogPage();	 
		 blogPage.clickOnFirstBlogAndAssertHeading();
		// blogPage.assertNextNavigationClass();
		 blogPage.clickPreviousBlogNavigation();
		 blogPage.assertNextNavigationClassAfterClickingPrevious();
		 blogPage.clickOnTheBlog();
		 blogPage.verifyBlogPage();
  }
  
  @Test
  public void tc043_collective_CategoriesAndBrandsUIVerifications(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  homePage.verifyProductURLsPages();
  }
  
  @Test
  public void tc044_collective_Registration_ErrorScenario_WholeNumbers_Only_Name() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsernameForWholeNumberData();
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  //pending due to a bug
	  signupPage.verifyErrorMsg_ErrorScenario_WholeNumbers_Name();
	
  }
  
  @Test
  public void tc045_collective_Registration_ErrorScenario_SpecialCharacters_Only_Name() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsernameForSpecialCharacterData();
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  //pending due to a bug
	  signupPage.verifyErrorMsg_ErrorScenario_SpecialCharacters_Name();
	
  }
  
  
  @Test
  public void tc046_collective_Registration_ErrorScenario_ExistingEmail() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsernameForSpecialCharacterData();
	  signupPage.signupEmailExisting();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsg_ExistingEmail();
  }
  

  @Test
  public void tc047_collective_Registration_ErrorScenario_PhoneNumber_LessThan10Characters() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsername();
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumberLessThan10();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsg_PhoneNumber_LessThan10Characters();
  }
 
  @Test
  public void tc048_collective_Registration_ErrorScenario_PhoneNumber_SpecialCharactersAndAlphabets() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsername();
	  signupPage.signupEmail();
	  signupPage.signupPhoneNumberSpecialCharactersAndAlphabets();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsg_PhoneNumber_LessThan10Characters();
  }
  
  @Test
  public void tc049_collective_Registration_ErrorScenario_Email_invalidEmailFormat() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveSignupPageObjects signupPage = new CollectiveSignupPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.emailSignUpBtnClick();	  
	  signupPage.signupUsername();
	  signupPage.signupEmailWrongFormat();
	  signupPage.signupPhoneNumber();
	  signupPage.signUpPasswordAndConfirmation();
	  signupPage.signupBtnClick();
	  signupPage.verifyErrorMsg_Email_invalidEmailFormat();
  }
  
  @Test
  public void tc050_collective_AddToCart_HugeNumber() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToBags();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.addAHugeQuantity();
	  andrianoPage.clickOnAddToCart();
	  andrianoPage.verifyErrorMsgForHugeNumberOfQuantity();  
  }
  
  @Test
  public void tc051_collective_AddToCart_negativeNumber() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToBags();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.addANegativeNumber();
	  andrianoPage.clickOnAddToCart();  
	  andrianoPage.verifyErrorMsgForNegativeNumberOfQuantity(); 	 
  }
  
  
  @Test
  public void tc052_collective_AddToCart_DecimalValue() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToBags();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.addDecimalValue();
	  andrianoPage.clickOnAddToCart();
	  andrianoPage.verifyErrorMsgForNegativeNumberOfQuantity(); 	 
  }
  
  @Test
  public void tc053_collective_Made_to_Measure_InitialPage() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  Assert.assertTrue(madeToMeasurePage.assertMadeToMeasureFirstPage(), "made to measure initial page text is invalid");
  }
  
  @Test
  public void tc054_collective_Made_to_MeasurePage() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickMadeToMeasure();
	 Assert.assertTrue(madeToMeasurePage.assertMadeToMeasurePage(), "made to measure page text is invalid");
  }
  
  @Test
  public void tc055_collective_Made_To_Measure_CollectiveAtelier() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	 Assert.assertTrue(madeToMeasurePage.assertCollectiveAtelierPageText(), "collective Atelier text is invalid");
  }
  
  @Test
  public void tc56_collective_Made_To_Measure_CollectiveAtelier_Armani() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickArmani();
	  Assert.assertTrue(madeToMeasurePage.assertArmaniPage(), "Armani page text is invalid");
	  
  }
  
  
  
  @Test
  public void tc057_collective_Made_To_Measure_CollectiveAtelier_Santoni() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickSantoni();
	  madeToMeasurePage.assertSantoniPage();
	  Assert.assertTrue(madeToMeasurePage.assertSantoniPageTabletView(),"Santoni page text is invalid");
  }
  

  @Test
  public void tc058_collective_Made_To_Measure_CollectiveAtelier_Eton() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickEton();
	  Assert.assertTrue(madeToMeasurePage.assertEtonPageText(), "Eton page text is invalid");
  }
  
  @Test
  public void tc059_collective_Made_To_Measure_CollectiveAtelier_Hackett() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  
	  madeToMeasurePage.clickHackett();
	  Assert.assertTrue(madeToMeasurePage.assertHackettPage(), "Hackett page text is invalid");
  }
  
  @Test
  public void tc060_collective_Made_To_Measure_Appointments() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  Assert.assertTrue(madeToMeasurePage.assertScheduleTable(), "schedule table is not displayed");
	 Assert.assertTrue( madeToMeasurePage.assertAppointmentPageTabletView(), "appointments page text is invalid");
  }
 
  @Test
  public void tc061_collective_Made_To_Measure_MakeAnAppointmentFunctionality() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(), "make an appointment confirmation is not displayed");
  }
  
  @Test
  public void tc062_collective_Made_To_Measure_MakeAnAppointmentFunctionality_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	 
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"make an appointment confirmation is not displayed");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc063_collective_Made_To_Measure_Appointments_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  Assert.assertTrue(madeToMeasurePage.assertScheduleTable(), "schedule table is not displayed");
	  madeToMeasurePage.assertAppointmentPage();
	  
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc064_collective_Made_To_Measure_CollectiveAtelier_Hackett_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickHackett();
	  Assert.assertTrue(madeToMeasurePage.assertHackettPage(), "hackett page text is invalid in mobile view");
	 
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc065_collective_Made_To_Measure_CollectiveAtelier_Eton_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  
	  madeToMeasurePage.clickEton();
	  Assert.assertTrue(madeToMeasurePage.assertEtonPageText(), "Eton page text is invalid in mobile view");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc066_collective_Made_To_Measure_CollectiveAtelier_Santoni_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	 
	  madeToMeasurePage.clickSantoni();
	  
	  Assert.assertTrue(madeToMeasurePage.assertSantoniPageTabletView(),"Santoni page text is invalid in mobile view");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc067_collective_Made_to_Measure_InitialPage_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	 Assert.assertTrue(madeToMeasurePage.assertMadeToMeasureFirstPage(), "made to measure initial page in mobile view is invalid!");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc068_collective_Made_to_MeasurePage_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickMadeToMeasure();
	  madeToMeasurePage.assertMadeToMeasurePage();
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc069_collective_Made_To_Measure_CollectiveAtelier_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  Assert.assertTrue(madeToMeasurePage.assertCollectiveAtelierPageMobileView(), "collective atelier in mobile view is invalid");
	  // homePage.maximizeWindow();
  }
  
  @Test
  public void tc070_collective_Made_To_Measure_CollectiveAtelier_Armani_MobileView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToMobileView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickArmani();
	  Assert.assertTrue(madeToMeasurePage.assertArmaniPage(), "Armani text in mobile view is invalid");
	  // homePage.maximizeWindow();
  }
  
  @Test
  public void tc071_collective_Made_to_Measure_InitialPage_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  Assert.assertTrue(madeToMeasurePage.assertMadeToMeasureFirstPage(), "Initial page in tablets view is invalid");
	 // homePage.maximizeWindow();
  }
  
  @Test
  public void tc072_collective_Made_to_MeasurePage_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickMadeToMeasure();
	  Assert.assertTrue(madeToMeasurePage.assertMadeToMeasurePage(), "Made to measure page in tablet view is invalid");
	 // homePage.maximizeWindow();
  }
  
  @Test
  public void tc073_collective_Made_To_Measure_CollectiveAtelier_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	 Assert.assertTrue(madeToMeasurePage.assertHackettLink(), "hackett link is not displayed");
	 Assert.assertTrue(madeToMeasurePage.assertEtonLink(), "Eton link is not displayed");
	 Assert.assertTrue(madeToMeasurePage.assertSantoniLink(), "Santoni link is not displayed");
	 Assert.assertTrue(madeToMeasurePage.assertArmaniLink(), "Armani link is not displayed");
	 Assert.assertTrue(madeToMeasurePage.assertCollectiveAtelierPageText(), "CollectiveAtelier text in tablet view is invalid");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc074_collective_Made_To_Measure_CollectiveAtelier_Armani_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickArmani();
	  Assert.assertTrue(madeToMeasurePage.assertArmaniPageTabletView(),"Armani page text is invalid");
	  
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc075_collective_Made_To_Measure_CollectiveAtelier_Santoni_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  Assert.assertTrue(madeToMeasurePage.assertHackettLink(), "hackett link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertEtonLink(), "Eton link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertSantoniLink(), "Santoni link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertArmaniLink(), "Armani link is not displayed");
	  madeToMeasurePage.clickSantoni();
	  Assert.assertTrue(madeToMeasurePage.assertSantoniPageTabletView(),"Santoni page text is invalid");
	 
	  //homePage.maximizeWindow();
  }
  

  @Test
  public void tc076_collective_Made_To_Measure_CollectiveAtelier_Eton_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  Assert.assertTrue(madeToMeasurePage.assertHackettLink(), "hackett link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertEtonLink(), "Eton link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertSantoniLink(), "Santoni link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertArmaniLink(), "Armani link is not displayed");
	  madeToMeasurePage.clickEton();
	  Assert.assertTrue(madeToMeasurePage.assertEtonPageText(),"Eton page text is invalid");
	  
	  
	
	  
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc077_collective_Made_To_Measure_CollectiveAtelier_Hackett_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  Assert.assertTrue(madeToMeasurePage.assertHackettLink(), "hackett link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertEtonLink(), "Eton link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertSantoniLink(), "Santoni link is not displayed");
	  Assert.assertTrue(madeToMeasurePage.assertArmaniLink(), "Armani link is not displayed");
	  madeToMeasurePage.clickHackett();
	  Assert.assertTrue(madeToMeasurePage.assertHackettPage(),"Hackett page text is invalid");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc078_collective_Made_To_Measure_Appointments_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  Assert.assertTrue(madeToMeasurePage.assertScheduleTable(), "schedule table is not displayed");
	  madeToMeasurePage.assertAppointmentPage();
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc079_collective_Made_To_Measure_MakeAnAppointmentFunctionality_TabletView() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.changeToTabletView();
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"confirmation text is not provided");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc080_collective_Made_To_Measure_CollectiveAtelier_Armani_MakeAnAppointment() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickArmani();
	  madeToMeasurePage.ClickMakeAnAppointButton();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"confirmation text is not provided");
	  //homePage.maximizeWindow();
  }
  
  @Test
  public void tc081_collective_Made_To_Measure_CollectiveAtelier_Armani_WatchVideo() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  YoutubePageObjects youtubePage = new YoutubePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickArmani();
	  madeToMeasurePage.ClickWatchVideoButton();
	  madeToMeasurePage.switchToYoutube();
	 Assert.assertTrue(youtubePage.assertArmaniVideoHeading(),"youtube Armani heading is not right"); 
	  
	  
  }
  
  @Test
  public void tc082_collective_Made_To_Measure_CollectiveAtelier_Santoni_MakeAnAppointment() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickSantoni();
	  madeToMeasurePage.ClickMakeAnAppointButton();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"confirmation text is not provided");
  }
  
  @Test
  public void tc083_collective_Made_To_Measure_CollectiveAtelier_Santoni_WatchVideo() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  YoutubePageObjects youtubePage = new YoutubePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickSantoni();
	  madeToMeasurePage.assertSantoniPage();
	  madeToMeasurePage.ClickWatchVideoButton();
	  madeToMeasurePage.switchToYoutube();
	  Assert.assertTrue(youtubePage.assertSantoniVideoHeading(),"youtube Santoni heading is not right");
  }
  
  @Test
  public void tc084_collective_Made_To_Measure_CollectiveAtelier_Hackett_MakeAnAppointment() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickHackett();
	  madeToMeasurePage.ClickMakeAnAppointButton();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"confirmation text is not provided");
  }
  
  @Test
  public void tc085_collective_Made_To_Measure_CollectiveAtelier_Eton_MakeAnAppointment() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.clickCollectiveAtelier();
	  madeToMeasurePage.assertHackettLink();
	  madeToMeasurePage.assertEtonLink();
	  madeToMeasurePage.assertSantoniLink();
	  madeToMeasurePage.assertArmaniLink();
	  madeToMeasurePage.clickEton();
	  madeToMeasurePage.ClickMakeAnAppointButton();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentFunctionality(),"confirmation text is not provided");
	  
  }
 
  
  @Test
  public void tc086_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_InvalidEmail() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterInvalidEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentErrorMsg(data.getAppointmentEmailErrorMsg()),"Invalid email error msg is not valid");
  }
  
  @Test
  public void tc087_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_InvalidPhoneNumber() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterInvalidPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentErrorMsg(data.getAppointmentPhoneNumberErrorMsg()),"Invalid phone number message is wrong");
  }
  
  @Test
  public void tc088_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_AllBlank() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentAllErrorMsgs(),"error messages are not right");
  }
  
  @Test
  public void tc089_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_NameBlank() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentErrorMsg(data.getAppointmentNameBlankErrorMsg()));
  }
  
  @Test
  public void tc090_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_EmailBlank() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterPhoneNumber();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentErrorMsg(data.getAppointmentEmailBlankErrorMsg()),"email blank error msg is invalid");
  }
  
  @Test
  public void tc091_collective_Made_To_Measure_ErrorScenario_MakeAnAppointmentFunctionality_PhoneNumberBlank() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	  homePage.clickMadeToMeasureLink();
	  homePage.switchToOtherTab();
	  madeToMeasurePage.clickAppointments();
	  madeToMeasurePage.enterName();
	  madeToMeasurePage.enterEmail();
	  madeToMeasurePage.clickSubmit();
	  Assert.assertTrue(madeToMeasurePage.assertAppointmentErrorMsg(data.getAppointmentPhoneNumberBlankErrorMsg()), "Phone number blank error message is not invalid");
  }
  
  @Test
  public void tc092_collective_OrderPlacement_newArrivals() throws IOException, AWTException, InterruptedException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.navigateTo_newArrivals();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  //andrianoPage.verifySoldOutProductAndClickNextProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
  }
  
  @Test(alwaysRun = true)
  public void tc093_collective_placeorder_sales() throws ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.clickSales();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  
  }
  
  @Test(alwaysRun = true)
  public void tc094_collective_placeorder_OrderCancellation() throws ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToArmaniFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
	  shoppingcartPage.enterPinCode();
	  shoppingcartPage.enterCity();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.clickPlaceOrder();
	  shoppingcartPage.verifyOrderStatus();
	  shoppingcartPage.clickCancel();
	  storesPage.switchToAlertAndAccept();
	  shoppingcartPage.verifyOrderCancellation();
  }
  
  @Test(alwaysRun = true)
  public void tc095_collective_shoppingCartAddress_ErrorPincode() throws ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToArmaniFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.enterFirstName();
	  shoppingcartPage.enterLastName();
	  shoppingcartPage.enterStreetAddress();
      shoppingcartPage.enterCity();
	  shoppingcartPage.enterInvalidPinCode();
	  shoppingcartPage.chooseState();
	  shoppingcartPage.chooseCountry();
	  shoppingcartPage.enterCartPhoneNumber(); 
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  //yet to be implemented. Method not defined yet
	  shoppingcartPage.verifyPinCodeError();
	  shoppingcartPage.clearCart();
  }
  
  @Test(alwaysRun = true)
  public void tc096_collective_shoppingCartAddress_allDataEmpty() throws ParseException, IOException{
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
	  CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
	  homePage.hoverOverMyAccount();
	  homePage.clickLoginLink();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  homePage.verifySignupFunctionality();
	  homePage.hoverAccessories();
	  homePage.hoverAccessoriesMen();
	  homePage.navigateToArmaniFromMen();
	  bagsPage.verifyProductsDisplay();
	  andrianoPage.clickOnFirstProduct();
	  andrianoPage.clickOnAddToCart();
	  shoppingcartPage.clickCheckout();
	  shoppingcartPage.clearAllFields();
	  shoppingcartPage.cartSaveMyAddress();
	  shoppingcartPage.saveAndContinueClick();
	  shoppingcartPage.verifyErrorMsgs();
	  shoppingcartPage.clearCart();
  }
  
  @Test
  public void tc097_collective_homePage_newSubscribeNewsLetter(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.enterSubscriptionEmail();
	  homePage.clickSubscribe();
	  homePage.verifySubscribeNewsLetterMsg();
  }
  
  @Test
  public void tc098_collective_homePage_exisitingSubscribeNewsLetter(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.enterExisitingSubscriptionEmail();
	  homePage.clickSubscribe();
	  homePage.verifySubscribeNewsLetterMsgExistingEmail();
  }
  
  @Test
  public void tc099_collective_homePage_emptyForSubscription(){
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
	  homePage.clickSubscribe();
	  homePage.verifyErrorMsgForNoEmailInSubscription();
  }
  
  
}