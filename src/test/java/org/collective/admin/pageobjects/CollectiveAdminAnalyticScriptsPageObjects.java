package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminAnalyticScriptsPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminAnalyticScriptsPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//a[@id='admin_new_analytic_scripts_link']")
	private WebElement newAnalyticScripts;
	
	@FindBy(xpath="//h1")
	private WebElement analyticScriptsHeading;

	@FindBy(xpath="//input[@id='analytic_script_name']")
	private WebElement analyticScriptName;
	
	@FindBy(xpath="//button[contains(text(),'Create')]")
	private WebElement createButton;
	
	@FindBy(css="a[href='/admin/analytic_scripts']")
	private WebElement analyticScriptsLink;
	
    
	String deleteButton = "//td[text()='"+data.getAnalyticScriptCode()+"']/following-sibling::td[@class='actions']/a[@data-method='delete']";

	public void verifyAnalyticScriptPage(){
		Waiting.explicitWaitVisibilityOfElement(analyticScriptsHeading, 3);
			Assert.assertTrue(analyticScriptsHeading.isDisplayed(), "analytic scripts heading is not displayed");
			Assert.assertTrue(newAnalyticScripts.isDisplayed(), "analytic scripts button is not displayed");
		}


	public void clickAddAnalyticScript() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",newAnalyticScripts);
	}


	public void enterAnalyticScriptName() {
		analyticScriptName.sendKeys(data.getAnalyticScriptName());
		
	}


	public void enterAnalyticScriptCode() {
		analyticScriptName.sendKeys(Keys.TAB,data.getAnalyticScriptCode());
		
	}


	public void clickCreate() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",createButton);
		
	}		
	
	public void clickDelete(){
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath(deleteButton)).click();
	}


	public void clickAnalyticScript() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",analyticScriptsLink);
		
	}
	}
