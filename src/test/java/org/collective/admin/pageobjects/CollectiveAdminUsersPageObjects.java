package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminUsersPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminUsersPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//h1[contains(text(),'Listing Users')]")
	private WebElement usersHeader;
	
	@FindBy(xpath="//a[@id='admin_new_user_link']")
	private WebElement newUserButton;
	
	@FindBy(xpath="//aside[@id='sidebar']")
	private WebElement searchSection;
	
	@FindBy(xpath="//input[@id='q_phone_number_or_email_cont']")
	private WebElement searchTextBox;
	
	@FindBy(xpath="//button[@name='button' and text()='Search']")
	private WebElement searchButton;
	
	@FindBy(xpath="//input[@id='user_email']")
	private WebElement emailID;
	
	@FindBy(xpath="//input[@id='user_password']")
	private WebElement password;
	
	@FindBy(xpath="//input[@id='user_password_confirmation']")
	private WebElement passwordConfirmation;
	
	@FindBy(xpath="//input[@id='user_phone_number']")
	private WebElement phoneNumber;
	
	@FindBy(xpath="//input[@id='user_spree_role_user']")
	private WebElement userCheckbox;
	
	@FindBy(xpath="//button[contains(text(),'Create')]")
	private WebElement createButton;
	
	@FindBy(xpath="//span[contains(text(),'Cancel')]")
	private WebElement cancelButton;
	
	@FindBy(xpath="//div[@class='flash success']")
	public WebElement alertSuccessMsg;
	
	@FindBy(xpath="//a[text()='Back To Users List']")
	private WebElement backToUsersList;
	
	@FindBy(xpath="//input[@id='q_phone_number_or_email_cont']")
	private WebElement searchBox;
	
	@FindBy(xpath="(//a[@data-action='remove'])[1]")
	private WebElement deleteButton;
	
	@FindBy(xpath="//input[@id='user_spree_role_admin']")
	private WebElement adminCheckBox;
	
	@FindBy(xpath="//input[@id='user_spree_role_blogger']")
	private WebElement bloggerCheckBox;
	
	public void verifyUsersPage() {
		Waiting.explicitWaitVisibilityOfElement(usersHeader, 6);
		Assert.assertTrue(usersHeader.isDisplayed());
		Assert.assertTrue(newUserButton.isDisplayed());
		Assert.assertTrue(searchSection.isDisplayed());
		Assert.assertTrue(searchTextBox.isDisplayed());
		Assert.assertTrue(searchButton.isDisplayed());
	}

	public void clickNewUser() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",newUserButton);
	}

	public void enterEmailID() {
		String s[]=data.getsignupEmail().split("@");
		String name = s[0];
		String domain = s[1];
		emailID.sendKeys(name+"12345@"+domain);
		
	}

	public void enterPassword() {
		password.sendKeys(data.getsignInPassword());
	}

	public void enterConfirmPassword() {
		passwordConfirmation.sendKeys(data.getsignInPassword());
		
	}
	
	public void enterUniquePhoneNumber(){
		phoneNumber.sendKeys(data.getUserCreatePhoneNumber());
	}

	public void clickUserCheckbox() {
		
		userCheckbox.click();
	}

	public void clickCreate() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",createButton);
	}
	
	public void verifyCreationOfUser(){
		Assert.assertEquals(alertSuccessMsg.getText().trim(), data.getUserCreateSuccessMsg());
	}

	public void clickBackToUsersList() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",backToUsersList);
		
	}

	public void searchForEmailID() {
		String s[]=data.getsignupEmail().split("@");
		String name = s[0];
		String domain = s[1];
		searchBox.sendKeys(name+"12345@"+domain);
		
	}
	
	public void clickSearchButton(){
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",searchButton);
	}
	
	public void clickDeleteButton(){
		Waiting.explicitWaitVisibilityOfElement(deleteButton, 5);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",deleteButton);
	}

	public void verifyUserDelete() {
		Waiting.explicitWaitVisibilityOfElement(alertSuccessMsg, 5);
		Assert.assertEquals(alertSuccessMsg.getText().trim(), data.getUserDeleteSuccessMsg());
		
	}

	public void clickAdminCheckbox() {
		adminCheckBox.click();
		
	}

	public void clickBloggerCheckbox() {
		bloggerCheckBox.click();
		
	}

	public void clickCancel() {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",cancelButton);
	}
	}

	
