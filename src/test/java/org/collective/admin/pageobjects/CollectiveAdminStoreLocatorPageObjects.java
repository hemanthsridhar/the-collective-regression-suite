package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminStoreLocatorPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminStoreLocatorPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//h1[contains(text(),'STORES')]")
	private WebElement storesHeader;
	
	@FindBy(xpath="//a[@id='admin_new_user_link']")
	private WebElement newStoreButton;
	
	@FindBy(xpath="//div[@class='row']")
	private WebElement storeTable;
	
	@FindBy(xpath="//input[@id='store_locator_city']")
	private WebElement cityTextBox;
	
	@FindBy(xpath="//input[@id='store_locator_address1']")
	private WebElement address1TextBox;
	
	
	@FindBy(xpath="//input[@id='store_locator_address2']")
	private WebElement address2TextBox;
	
	@FindBy(xpath="//input[@id='store_locator_address3']")
	private WebElement address3TextBox;
	
	@FindBy(xpath="//input[@id='store_locator_phone_number']")
	private WebElement phoneNumberTextBox;
	
	@FindBy(xpath="//button[text()='Create']")
	private WebElement createButton;
	
	@FindBy(xpath="(//td[@class='align-center'])[last()]")
	private WebElement lastStoreNameAfterCreation;
	
	@FindBy(xpath="(//a[@class='fa fa-edit icon_link with-tip no-text'])[last()]")
	private WebElement lastStoreEditSymbol;
	
	@FindBy(xpath="(//a[@class='delete-resource fa fa-trash icon_link with-tip no-text'])[last()]")
	private WebElement lastDeleteButton;
	
	@FindBy(xpath="//button[text()='Update']")
	private WebElement updateButton;
	
	@FindBy(xpath="//a[@href='/admin/store_locator']")
	private WebElement backToStoreList;
	
	public void verifyStoresPage() {
		Waiting.explicitWaitVisibilityOfElement(storesHeader, 6);
		Assert.assertTrue(storesHeader.isDisplayed());
		Assert.assertTrue(newStoreButton.isDisplayed());
		Assert.assertTrue(storeTable.isDisplayed());
	}



	public void clickNewStoreButton() {
		 ((JavascriptExecutor)driver).executeScript("arguments[0].click()",newStoreButton);
		
	}



	public void enterCity() {
		cityTextBox.sendKeys(data.getCreateStoreCity());
		
	}



	public void enterAddress1() {
		address1TextBox.sendKeys(data.getCreateStoreAddress1());
		
	}



	public void enterAddress2() {
		// TODO Auto-generated method stub
		address2TextBox.sendKeys(data.getCreateStoreAddress2());
	}



	public void enterPhoneNumber() {
		// TODO Auto-generated method stub
		phoneNumberTextBox.sendKeys(data.getsignInPhoneNumber());
	}



	public void clickCreate() {
		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",createButton);
	}



	public void verifyStoreCreate() {
		
		Assert.assertEquals(lastStoreNameAfterCreation.getText().trim(),data.getCreateStoreCity());
		
		
	}



	public void deleteStore() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click()",lastDeleteButton);
		
	}



	public void assertAfterDelete() {
		Assert.assertNotEquals(lastStoreNameAfterCreation.getText().trim(),data.getCreateStoreCity(), "issue after deleting the store");
	}



	public void switchToAlertAndAccept() {
		
		Waiting.explicitWaitForAlert(5);
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		
	}



	public void clickNewCreatedStore() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click()",lastStoreEditSymbol);
		
	}



	public void enterAddress3() {
		
		address3TextBox.sendKeys(data.getCreateStoreAddress1());
	}



	public void clickUpdate() {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",updateButton);
	}



	public void clickBackToStoresList() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",backToStoreList);
		
	}



	public void verifyUpdate() {
		Waiting.explicitWaitVisibilityOfElement(address3TextBox, 4);
		Assert.assertEquals(address3TextBox.getAttribute("value").trim(),data.getCreateStoreAddress1());
		
	}
	}

	
